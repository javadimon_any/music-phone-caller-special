package ru.any.projects.mpcs.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.concurrent.Executors;

import ru.any.projects.mpcs.R;

import static android.media.AudioManager.AUDIOFOCUS_GAIN;
import static android.media.AudioManager.AUDIOFOCUS_LOSS;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;

public class MusicService extends Service implements AudioManager.OnAudioFocusChangeListener {

    private MediaPlayer mediaPlayer;
    private AudioFocusRequest mAudioFocusRequest;
    private AudioManager mAudioManager;
    private BroadcastReceiver broadcastReceiver;
    private static final String TAG = "MUSIC SERVICE DEBUG";

    @Override
    public void onCreate() {
        Log.d("MusicService", "onCreate");

        mAudioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        //int volume = mAudioManager.getStreamMaxVolume(mAudioManager.STREAM_MUSIC) / 2;
        //mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_SHOW_UI);

        AudioAttributes mAudioAttributes =
                new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build();

        mAudioFocusRequest =
                new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                        .setAudioAttributes(mAudioAttributes)
                        .setAcceptsDelayedFocusGain(true)
                        .setOnAudioFocusChangeListener(this)
                        .build();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String command = intent.getStringExtra("Command");
                Log.d("MusicService DEBUG", command);

                if(command == null){
                    return;
                }

                switch (command){
                    case "Stop":
                        if(mediaPlayer != null){
                            mediaPlayer.stop();
                            //stopService();
                        }
                        break;

                    case "Play":
                        Executors.newSingleThreadExecutor().submit(playMusic(getCurrentContext()));
                        mAudioManager.requestAudioFocus(mAudioFocusRequest);
                        break;
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("ru.any.projects.mpcs.services.MusicService");
        registerReceiver(broadcastReceiver, filter);

        //Executors.newSingleThreadExecutor().submit(playMusic(this));

    }

    private Context getCurrentContext(){
        return this;
    }

    private void stopService(){
        unregisterReceiver(broadcastReceiver);
        this.stopSelf();
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AUDIOFOCUS_GAIN:
                Log.d(TAG, "AUDIOFOCUS_GAIN");
                break;

            case AUDIOFOCUS_LOSS:
                Log.d(TAG, "AUDIOFOCUS_LOSS");
                break;

            case AUDIOFOCUS_LOSS_TRANSIENT:
                Log.d(TAG, "AUDIOFOCUS_LOSS_TRANSIENT");
                int focusRequest = mAudioManager.requestAudioFocus(mAudioFocusRequest);
                Log.d(TAG, "focusRequest: " + focusRequest);
                break;
        }
    }

    @Override
    public void onDestroy() {
        if(mediaPlayer != null){
            mediaPlayer.stop();
        }
    }

    private Runnable playMusic(final Context context){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                mediaPlayer = MediaPlayer.create(context, R.raw.song);
                mediaPlayer.start();
            }
        };

        return runnable;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
